# Create TodoMVC Application with REACT.js in Typescript

we'll create the following directory structure, files and their contents:

TodoMVC project

```
|- package.json
|- package-lock.json
|- index.html
|- /node_modules
|- /src
  |- index.js
  |- Interfaces.ts
  |- TodoStore.ts
  |- main.tsx
|- tslint.json
|- tsconfig.json
|- webpack.config.js
```

- URL documentation :
  - More details on [webpack](https://webpack.js.org/guides/development/) documentation

## Install & configure Dependencies

First let's create a directory, initialize app with npm:

```sh
mkdir TodoMVC-React-TypeScript
cd TodoMVC-React-TypeScript
```

- I don't use `babel`, I will use `Webpack`

- Init `Node.js` application with `npm`, `y` flag answer "yes" to all questions, run :

  install webpack locally, and install the webpack-cli (the tool used to run webpack on the command line):

```sh
npm init -y
npm install webpack webpack-cli --save-dev
```

- create a `webpack.config.js` file :
  `touch webpack.config.js`

* src/index.js

```js
function component() {
  const element = document.createElement("div");

  element.innerHTML = _.join(["Hello", "webpack"], " ");

  return element;
}

document.body.appendChild(component());
```

- index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Getting Started</title>
    <script src="https://unpkg.com/lodash@4.17.20"></script>
  </head>
  <body>
    <script src="./src/index.js"></script>
  </body>
</html>
```

## Creating a Bundle

- First we'll tweak our directory structure slightly,
- separating the "source" code (./src) from our "distribution" code (./dist).
- The "source" code is the code that we'll write and edit.
- The "distribution" code is the minimized and optimized output of our build process that will eventually be loaded in the browser. Tweak the directory structure as follows:

```js
|- package.json
|- package-lock.json
 |- /dist
  |- index.html
|- index.html
 |- /src
  |- index.js
```

- Tip
  You may have noticed that index.html was created manually, even though it is now placed in the dist directory. Later on in another guide, we will generate index.html rather than edit it manually. Once this is done, it should be safe to empty the dist directory and to regenerate all the files within it.

To bundle the lodash dependency with index.js, we'll need to install the library locally:

```sh
npm install --save lodash
```

- Tip
  When installing a package that will be bundled into your production bundle, use
  `npm install --save`.

If you're installing a package for development purposes (e.g. a linter, testing libraries, etc.) then you should use `npm install --save-dev`.

Now, let's import lodash in our script:

src/index.js

```js
import _ from "lodash";

function component() {
  const element = document.createElement("div");

  // Lodash, currently included via a script, is required for this line to work
  // Lodash, now imported by this script
  element.innerHTML = _.join(["Hello", "webpack"], " ");

  return element;
}

document.body.appendChild(component());
```

- Now, since we'll be bundling our scripts, we have to update our index.html file. Let's remove the lodash <script>, as we now import it, and modify the other <script> tag to load the bundle, instead of the raw ./src file:

dist/index.html

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Getting Started</title>
    <script src="https://unpkg.com/lodash@4.17.20"></script>
  </head>
  <body>
    <script src="./src/index.js"></script>
    <script src="main.js"></script>
  </body>
</html>
```

In this setup, `index.js` explicitly requires lodash to be present, and binds it as \_ (no global scope pollution). By stating what dependencies a module needs, webpack can use this information to build a dependency graph. It then uses the graph to generate an optimized bundle where scripts will be executed in the correct order.

With that said, let's run `npx webpack`, which will take our script at src/index.js as the entry point, and will generate dist/main.js as the output. The npx command, which ships with Node 8.2/npm 5.2.0 or higher, runs the webpack binary (./node_modules/.bin/webpack) of the webpack package we installed in the beginning:

```sh
$ npx webpack
[webpack-cli] Compilation finished
asset main.js 69.3 KiB [emitted] [minimized] (name: main) 1 related asset
runtime modules 1000 bytes 5 modules
cacheable modules 530 KiB
 ./src/index.js 257 bytes [built] [code generated]
 ./node_modules/lodash/lodash.js 530 KiB [built] [code generated]
webpack 5.4.0 compiled successfully in 1851 ms
```

- Tip
  Your output may vary a bit, but if the build is successful then you are good to go.

Open index.html from the dist directory in your browser and, if everything went right, you should see the following text: 'Hello webpack'.

- Modules
  The import and export statements have been standardized in ES2015. They are supported in most of the browsers at this moment, however there are some browsers that don't recognize the new syntax. But don't worry, webpack does support them out of the box.

Behind the scenes, webpack actually "transpiles" the code so that older browsers can also run it. If you inspect dist/main.js, you might be able to see how webpack does this, it's quite ingenious! Besides import and export, webpack supports various other module syntaxes as well, see Module API for more information.

Note that webpack will not alter any code other than import and export statements. If you are using other ES2015 features, make sure to use a transpiler such as Babel via webpack's loader system.

- Using a Configuration
  As of version 4, webpack doesn't require any configuration, but most projects will need a more complex setup, which is why webpack supports a configuration file. This is much more efficient than having to manually type in a lot of commands in the terminal, so let's create one:

* webpack.config.js

```js
const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist"),
  },
};
```

Now, let's run the build again but instead using our new configuration file:

```sh
$ npx webpack --config webpack.config.js
[webpack-cli] Compilation finished
asset main.js 69.3 KiB [compared for emit] [minimized] (name: main) 1 related asset
runtime modules 1000 bytes 5 modules
cacheable modules 530 KiB
 ./src/index.js 257 bytes [built] [code generated]
 ./node_modules/lodash/lodash.js 530 KiB [built] [code generated]
webpack 5.4.0 compiled successfully in 1934 ms
```

Development
Tip
This guide extends on code examples found in the Output Management guide.

If you've been following the guides, you should have a solid understanding of some of the webpack basics. Before we continue, let's look into setting up a development environment to make our lives a little easier.

Warning
The tools in this guide are only meant for development, please avoid using them in production!

Let's start by setting mode to 'development' and title to 'Development'.

webpack.config.js

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
mode: 'development',
entry: {
index: './src/index.js',
print: './src/print.js',
},
plugins: [
new HtmlWebpackPlugin({
title: 'Output Management',
title: 'Development',
}),
],
output: {
filename: '[name].bundle.js',
path: path.resolve(\_\_dirname, 'dist'),
clean: true,
},
};
Using source maps
When webpack bundles your source code, it can become difficult to track down errors and warnings to their original location. For example, if you bundle three source files (a.js, b.js, and c.js) into one bundle (bundle.js) and one of the source files contains an error, the stack trace will point to bundle.js. This isn't always helpful as you probably want to know exactly which source file the error came from.

In order to make it easier to track down errors and warnings, JavaScript offers source maps, which map your compiled code back to your original source code. If an error originates from b.js, the source map will tell you exactly that.

There are a lot of different options available when it comes to source maps. Be sure to check them out so you can configure them to your needs.

For this guide, let's use the inline-source-map option, which is good for illustrative purposes (though not for production):

webpack.config.js

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
mode: 'development',
entry: {
index: './src/index.js',
print: './src/print.js',
},
devtool: 'inline-source-map',
plugins: [
new HtmlWebpackPlugin({
title: 'Development',
}),
],
output: {
filename: '[name].bundle.js',
path: path.resolve(\_\_dirname, 'dist'),
clean: true,
},
};
Now let's make sure we have something to debug, so let's create an error in our print.js file:

src/print.js

export default function printMe() {
console.log('I get called from print.js!');
cosnole.log('I get called from print.js!');
}
Run an npm run build, it should compile to something like this:

...
[webpack-cli] Compilation finished
asset index.bundle.js 1.38 MiB [emitted] (name: index)
asset print.bundle.js 6.25 KiB [emitted] (name: print)
asset index.html 272 bytes [emitted]
runtime modules 1.9 KiB 9 modules
cacheable modules 530 KiB
./src/index.js 406 bytes [built] [code generated]
./src/print.js 83 bytes [built] [code generated]
./node_modules/lodash/lodash.js 530 KiB [built] [code generated]
webpack 5.4.0 compiled successfully in 706 ms
Now open the resulting index.html file in your browser. Click the button and look in your console where the error is displayed. The error should say something like this:

Uncaught ReferenceError: cosnole is not defined
at HTMLButtonElement.printMe (print.js:2)
We can see that the error also contains a reference to the file (print.js) and line number (2) where the error occurred. This is great because now we know exactly where to look in order to fix the issue.

Choosing a Development Tool
Warning
Some text editors have a "safe write" function that might interfere with some of the following tools. Read Adjusting Your Text Editor for a solution to these issues.

It quickly becomes a hassle to manually run npm run build every time you want to compile your code.

There are a couple of different options available in webpack that help you automatically compile your code whenever it changes:

webpack's Watch Mode
webpack-dev-server
webpack-dev-middleware
In most cases, you probably would want to use webpack-dev-server, but let's explore all of the above options.

Using Watch Mode
You can instruct webpack to "watch" all files within your dependency graph for changes. If one of these files is updated, the code will be recompiled so you don't have to run the full build manually.

Let's add an npm script that will start webpack's Watch Mode:

package.json

{
"name": "webpack-demo",
"version": "1.0.0",
"description": "",
"private": true,
"scripts": {
"test": "echo \"Error: no test specified\" && exit 1",
"watch": "webpack --watch",
"build": "webpack"
},
"keywords": [],
"author": "",
"license": "ISC",
"devDependencies": {
"html-webpack-plugin": "^4.5.0",
"webpack": "^5.4.0",
"webpack-cli": "^4.2.0"
},
"dependencies": {
"lodash": "^4.17.20"
}
}
Now run npm run watch from the command line and see how webpack compiles your code. You can see that it doesn't exit the command line because the script is currently watching your files.

Now, while webpack is watching your files, let's remove the error we introduced earlier:

src/print.js

export default function printMe() {
cosnole.log('I get called from print.js!');
console.log('I get called from print.js!');
}
Now save your file and check the terminal window. You should see that webpack automatically recompiles the changed module!

The only downside is that you have to refresh your browser in order to see the changes. It would be much nicer if that would happen automatically as well, so let's try webpack-dev-server which will do exactly that.

Using webpack-dev-server
The webpack-dev-server provides you with a rudimentary web server and the ability to use live reloading. Let's set it up:

npm install --save-dev webpack-dev-server
Change your configuration file to tell the dev server where to look for files:

webpack.config.js

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
mode: 'development',
entry: {
index: './src/index.js',
print: './src/print.js',
},
devtool: 'inline-source-map',
devServer: {
static: './dist',
},
plugins: [
new HtmlWebpackPlugin({
title: 'Development',
}),
],
output: {
filename: '[name].bundle.js',
path: path.resolve(\_\_dirname, 'dist'),
clean: true,
},
};
This tells webpack-dev-server to serve the files from the dist directory on localhost:8080.

Tip
webpack-dev-server serves bundled files from the directory defined in output.path, i.e., files will be available under http://[devServer.host]:[devServer.port]/[output.publicPath]/[output.filename].

Warning
webpack-dev-server doesn't write any output files after compiling. Instead, it keeps bundle files in memory and serves them as if they were real files mounted at the server's root path. If your page expects to find the bundle files on a different path, you can change this with the devMiddleware.publicPath option in the dev server's configuration.

Let's add a script to easily run the dev server as well:

package.json

{
"name": "webpack-demo",
"version": "1.0.0",
"description": "",
"private": true,
"scripts": {
"test": "echo \"Error: no test specified\" && exit 1",
"watch": "webpack --watch",
"start": "webpack serve --open",
"build": "webpack"
},
"keywords": [],
"author": "",
"license": "ISC",
"devDependencies": {
"html-webpack-plugin": "^4.5.0",
"webpack": "^5.4.0",
"webpack-cli": "^4.2.0",
"webpack-dev-server": "^3.11.0"
},
"dependencies": {
"lodash": "^4.17.20"
}
}
Now we can run npm start from the command line and we will see our browser automatically loading up our page. If you now change any of the source files and save them, the web server will automatically reload after the code has been compiled. Give it a try!

The webpack-dev-server comes with many configurable options. Head over to the documentation to learn more.

Tip
Now that your server is working, you might want to give Hot Module Replacement a try!

Using webpack-dev-middleware
webpack-dev-middleware is a wrapper that will emit files processed by webpack to a server. This is used in webpack-dev-server internally, however it's available as a separate package to allow more custom setups if desired. We'll take a look at an example that combines webpack-dev-middleware with an express server.

Let's install express and webpack-dev-middleware so we can get started:

npm install --save-dev express webpack-dev-middleware
Now we need to make some adjustments to our webpack configuration file in order to make sure the middleware will function correctly:

webpack.config.js

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
mode: 'development',
entry: {
index: './src/index.js',
print: './src/print.js',
},
devtool: 'inline-source-map',
devServer: {
static: './dist',
},
plugins: [
new HtmlWebpackPlugin({
title: 'Development',
}),
],
output: {
filename: '[name].bundle.js',
path: path.resolve(\_\_dirname, 'dist'),
clean: true,
publicPath: '/',
},
};
The publicPath will be used within our server script as well in order to make sure files are served correctly on http://localhost:3000. We'll specify the port number later. The next step is setting up our custom express server:

project

webpack-demo
|- package.json
|- package-lock.json
|- webpack.config.js
|- server.js
|- /dist
|- /src
|- index.js
|- print.js
|- /node_modules
server.js

const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');

const app = express();
const config = require('./webpack.config.js');
const compiler = webpack(config);

// Tell express to use the webpack-dev-middleware and use the webpack.config.js
// configuration file as a base.
app.use(
webpackDevMiddleware(compiler, {
publicPath: config.output.publicPath,
})
);

// Serve the files on port 3000.
app.listen(3000, function () {
console.log('Example app listening on port 3000!\n');
});
Now add an npm script to make it a little easier to run the server:

`package.json`

```
 {
   "name": "webpack-demo",
   "version": "1.0.0",
   "description": "",
   "private": true,
   "scripts": {
     "test": "echo \"Error: no test specified\" && exit 1",
     "watch": "webpack --watch",
     "start": "webpack serve --open",
    "server": "node server.js",
     "build": "webpack"
   },
   "keywords": [],
   "author": "",
   "license": "ISC",
   "devDependencies": {
     "express": "^4.17.1",
     "html-webpack-plugin": "^4.5.0",
     "webpack": "^5.4.0",
     "webpack-cli": "^4.2.0",
     "webpack-dev-middleware": "^4.0.2",
     "webpack-dev-server": "^3.11.0"
   },
   "dependencies": {
     "lodash": "^4.17.20"
   }
 }
```

Now in your terminal run npm run server, it should give you an output similar to this:

Example app listening on port 3000!
...
<i> [webpack-dev-middleware] asset index.bundle.js 1.38 MiB [emitted] (name: index)
<i> asset print.bundle.js 6.25 KiB [emitted] (name: print)
<i> asset index.html 274 bytes [emitted]
<i> runtime modules 1.9 KiB 9 modules
<i> cacheable modules 530 KiB
<i> ./src/index.js 406 bytes [built] [code generated]
<i> ./src/print.js 83 bytes [built] [code generated]
<i> ./node_modules/lodash/lodash.js 530 KiB [built] [code generated]
<i> webpack 5.4.0 compiled successfully in 709 ms
<i> [webpack-dev-middleware] Compiled successfully.
<i> [webpack-dev-middleware] Compiling...
<i> [webpack-dev-middleware] assets by status 1.38 MiB [cached] 2 assets
<i> cached modules 530 KiB (javascript) 1.9 KiB (runtime) [cached] 12 modules
<i> webpack 5.4.0 compiled successfully in 19 ms
<i> [webpack-dev-middleware] Compiled successfully.
Now fire up your browser and go to http://localhost:3000. You should see your webpack app running and functioning!

T+ ip
If you would like to know more about how Hot Module Replacement works, we recommend you read the Hot Module Replacement guide.

#### Adjusting Your Text Editor

When using automatic compilation of your code, you could run into issues when saving your files. Some editors have a "safe write" feature that can potentially interfere with recompilation.

To disable this feature in some common editors, see the list below:

Sublime Text 3: Add atomic_save: 'false' to your user preferences.
JetBrains IDEs (e.g. WebStorm): Uncheck "Use safe write" in Preferences > Appearance & Behavior > System Settings.
Vim: Add :set backupcopy=yes to your settings.

## Environment Variables

To disambiguate in your `webpack.config.js` between development and production builds you may use environment variables.

- Tip
  webpack's environment variables are different from the environment variables of operating system shells like bash and CMD.exe

The webpack command line environment option --env allows you to pass in as many environment variables as you like. Environment variables will be made accessible in your `webpack.config.js`. For example, --env production or --env goal=local.

```
npx webpack --env goal=local --env production --progress
```

- Tip
  Setting up your env variable without assignment, -`-env` production sets env.production to true by default. There are also other syntaxes that you can use. See the webpack CLI documentation for more information.

There is one change that you will have to make to your webpack config. Typically, module.exports points to the configuration object. To use the env variable, you must convert module.exports to a function:

`webpack.config.js`

```js
const path = require("path");

module.exports = (env) => {
  // Use env.<YOUR VARIABLE> here:
  console.log("Goal: ", env.goal); // 'local'
  console.log("Production: ", env.production); // true

  return {
    entry: "./src/index.js",
    output: {
      filename: "bundle.js",
      path: path.resolve(__dirname, "dist"),
    },
  };
};
```

- Tip
  Webpack CLI offers some built-in environment variables which you can access inside a webpack configuration.

## Typescript

``

## TSLint, TSLoader

``

## Personnal recommendations on React, Preact & Typescript coding tips

- This project is a good step to work in TypeScript, React, Preact & Webpack.
- Warning:this project don't follow actually (After Year 2019), React recommandations (best practrice) coding.

This project was created in 2018, before `React` officials best practaces recommendation, to rather use `hooks` & functional componant.
Here, I used `class` componants.

## `yarn`

`npm i -D ts-loader tslint tslint-config-standard tslint-loader typescript webpack webpack-cli webpack-dev-server cross-env @types/react`

`yarn add -D ts-loader tslint tslint-config-standard tslint-loader typescript webpack webpack-cli webpack-dev-server cross-env @types/react`

`npm i react classnames`

## change scripts dev & build

- change scripts into `package.json` to easy acces commands dev & build

```json
"scripts": {
    "dev": "cross-env NODE_ENV=development webpack-dev-server --hot",
    "build": "cross-env NODE_ENV=production webpack"
  },
```

## Add Docker

## Add a DevOps CI/CD Pipeline (lint, tests, & deploy stages)

- We will create a Continuous Integretion ( CI/CD )

  - Goal : Automatisation all app stages like :
    - quality code with the linter (TSLint),
    - run tests (like unit, charge, end2end, Data Base ... test)
    - & finally, we will deploy this Single Page Application on the GitLab Pages.

- First, create a new file as `.gitlab-ci.yml`
- Next, include :
  - Docker image (see docker hub) & stages
    - node alpine (see also docker distroless image instead alpine)
  - add npm scripts
  - TSLint
  - Tests (uit, charge, end2end, Data Base ...)
  - Deploy (only on master)

## Add TSLint

- `tslint` config:

```

```

## GitLab App Versionning

Command line instructions
You can also upload existing files from your computer using the instructions below.

- Git global setup
  `git config --global user.name "hugues gouttebroze"`
  `git config --global user.email "hugues.gouttebrozze@etu.univ-st-etienne.fr"`

- Create a new repository
  `git clone git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `cd a-react-todo-in-typescript`
  `git switch -c main`
  `touch README.md`
  `git add README.md`
  `git commit -m "add README"`
  `git push -u origin main`

- Push an existing folder
  `cd existing_folder`
  `git init --initial-branch=main`
  `git remote add origin git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `git add .`
  `git commit -m "Initial commit"`
  `git push -u origin main`

- Push an existing Git repository
  `cd existing_repo`
  `git remote rename origin old-origin`
  `git remote add origin git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `git push -u origin --all`
  `git push -u origin --tags`

## Available Scripts

In the project directory, you can run:

- use `yarn` (faster than `npm`) when script works

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## GitLab App Versionning

Command line instructions
You can also upload existing files from your computer using the instructions below.

- Git global setup
  `git config --global user.name "hugues gouttebroze"`
  `git config --global user.email "hugues.gouttebrozze@etu.univ-st-etienne.fr"`

- Create a new repository
  `git clone git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `cd a-react-todo-in-typescript`
  `git switch -c main`
  `touch README.md`
  `git add README.md`
  `git commit -m "add README"`
  `git push -u origin main`

- Push an existing folder
  `cd existing_folder`
  `git init --initial-branch=main`
  `git remote add origin git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `git add .`
  `git commit -m "Initial commit"`
  `git push -u origin main`

- Push an existing Git repository
  `cd existing_repo`
  `git remote rename origin old-origin`
  `git remote add origin git@gitlab.com:HGouttebroze/a-react-todo-in-typescript.git`
  `git push -u origin --all`
  `git push -u origin --tags`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)
