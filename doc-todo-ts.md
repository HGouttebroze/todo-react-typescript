# Création du Store

- séparer la partie données dans une classe dédiée
  - pr ajouter plus de fonctionnalité plus tard (synchronisation sur un serveur, utilisation du localstorage...).
  - ReactJS n'impose pas de structure particulière, éviter les mutations ds la classe, les comparaisons seront + difficiles par la suite.

## Typescript config

- config TS on `.jsx` extension in `compilerOptions`, put `react` value on `.jsx` key like this :

`tsconfig.json`

```json
{
  "files": ["../TodoMVC-React-TypeScript/src/main.tsx"],
  "compilerOptions": {
    "target": "es5",
    "module": "es2015",
    "moduleResolution": "node",
    "sourceMap": true,
    "strict": true,
    "allowJs": true,
    "jsx": "react"
  },
  "exclude": ["node_modules"]
}
```

## Typescript Declaration file

- Typescript need a Declaration file

* By default, TS don't know react types,
  we must import some types, run :

`npm i -D @types/react @types/react-dom`

- on import des fichiers de déclarations qui expliques au TS comment fontionnent ces librairies (`react` & `react-dom`)
