import * as React from "react";
import TodoStore from "./TodoStore";
import { Todo } from "./Interfaces";
// import TodoItem from "./TodoItem";
import * as cx from "classnames";

export default class TodoList extends React.Component {
  // constructor() {
  //   super
  // }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" />
        </header>
        <section className="main">
          <input className="toggle-all" type="checkbox" />
          <label htmlFor="toggle-all">Mark all as complete</label>
          <ul className="todo-list"></ul>
        </section>
        <footer className="footer">
          <span className="todo-count">
            <strong></strong>
            left
          </span>

          <ul className="filters">
            <li>
              <a href="#/">All</a>
            </li>
            <li>
              <a href="#/active">Active</a>
            </li>
            <li>
              <a href="#/completed">Completed</a>
            </li>
          </ul>
        </footer>
      </section>
    );
  }
}
